package sheridan;
import java.lang.Math;

public class Fahrenheit {

	public static void main(String[] args) {
		
		
		System.out.println(convertFromCelsius(13));

	}
	
	public static int convertFromCelsius(int x) {
	
	float f;
	
	f = x * (9f / 5) + 32;
	int converted = (int) Math.ceil(f);
	
	return converted;
	
	}
	
}
