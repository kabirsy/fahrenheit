package sheridan;



import static org.junit.Assert.*;

import org.junit.Test;

public class FahrenheitTest {
	
	Fahrenheit f = new Fahrenheit();
	
	int result1 = f.convertFromCelsius(10); // expected outcome 50
	int result3 = f.convertFromCelsius(11); // 52
	int result4 = f.convertFromCelsius(13); // 56

	
	@Test
	public void testRegular() {
		assertEquals(result1, 50);
	}
	
	@Test
	public void testNegative() {
		// i tried but couldnt do this
	}
	
	@Test
	public void testBoundaryIn() {
		assertEquals(result3, 52);
	}
	
	@Test
	public void testBoundaryOut() {
		assertEquals(result4, 56);
	}

}
